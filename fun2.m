function [f,g] = fun2(beta,X,y)

    f = -sum(-exp(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])...
        +y.*(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])-log(factorial(y)));
    if nargout > 1 
        g =  0;
        for ii = 1:length(y)
         g = g  - (X(ii,:)'*(-exp(X(ii,:)*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)]))+X(ii,:)'*y(ii));
        end
    end
end