function fval = HW4_first(beta,X,Y,Z)
N = size(X,2);
pd = makedist('Normal',beta(1),(beta(2)));
beta_sim = randn(20,N)+beta(1);
beta_sim_pdf = pdf(pd,beta_sim);

Likelihood = zeros(1,N);
for ii = 1:20
Likelihood_it = ((1./(1+exp(-(beta_sim(ii,:).*X + beta(3).*Z )).^Y)).*...
    (1-1./(1+exp(-(beta_sim(ii,:).*X + beta(3).*Z)))).^(1-Y));
% not the right formula again. Take a look at answer key.
Likelihood_i = prod(Likelihood_it);
Likelihood = Likelihood + Likelihood_i.*beta_sim_pdf(ii,:);
end

fval = -sum(log(Likelihood));
end