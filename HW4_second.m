function fval = HW4_second(beta,X,Y,Z)
N = size(X,2);
pd = makedist('Normal',beta(1),beta(2));
beta_sim = rand(100,N);
beta_sim_pdf = pdf(pd,beta_sim);

Likelihood2 = zeros(1,N);
for ii = 1:100
Likelihood_it = (((1./(1+exp(-(beta_sim(ii,:).*X+beta(3).*Z)))).^Y).*...
    ((1-1./(1+exp(-(beta_sim(ii,:).*X+beta(3).*Z)))).^(1-Y)));
Likelihood_i = prod(Likelihood_it);
Likelihood2 = Likelihood2 + Likelihood_i.*beta_sim_pdf(ii,:);
end

fval = -sum(log(Likelihood2));
end