function pval = demand(p,v)
pval =1-p.*(1-(exp(v-p))./(1+sum(exp(v-p))));
end