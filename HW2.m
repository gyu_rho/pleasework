clear
addpath('C:/Users/Gyu/Documents/econ_512_2017/CEtools/')
% better to use the relative paths like ../econ_512_2017 etc
%Q1
e = exp(-1-(1));
qA = e/(1+3*e)
qB = e/(1+3*e)
qC = e/(1+3*e)
q0 = 1/(1+3*e)

%Q2
%% *Initial Guesses*
% better to break down into sections
tic
p    = [1;1;1]; %ones(3,1);     % Country Price index, initial value      
v    = -ones(3,1);     % Coefficients to be estimated, initial guess
q = [qA; qB; qC]; %initial guess
pnewq2    = p;
poldq2    = p+0.5;
j       = 1;

tol = 1e-11 ; 

% set up Broyden
optset('broyden','showiters',1000);
optset('broyden','maxit',300) ;
optset('broyden','tol',1e-11) ;

while norm(pnewq2-poldq2)>tol
   
p = pnewq2;
        
[p,pfval]  = broyden(@(x) demand(x,v),p);

poldq2=pnewq2;
pnewq2=p;
j = j+1;
end
toc
%% Q3

% *Initial Guesses*
tic
p    = [1;1;1]; %ones(3,1);     % Country Price index, initial value      
v    = -ones(3,1);     % Coefficients to be estimated, initial guess
j       = 1;

tol = 1e-11 ; 
pnewq3 = p;
poldq3 = p+5;
while (norm(poldq3-pnewq3)>tol )
     p = pnewq3;
for i = 1: 1000
    qA = exp(v(1)-p(1))/(1+sum(exp(v-p)));
    p(1) = 1/(1-qA);
    f1val = qA - exp(v(1)-p(1))/(1+sum(exp(v-p)));
    if abs(f1val)<tol
        break; 
    end
end
for i = 1: 1000
    qB = exp(v(2)-p(2))/(1+sum(exp(v-p)));
    p(2) = 1/(1-qB);
    f2val = qB - exp(v(2)-p(2))/(1+sum(exp(v-p)));
    if abs(f2val)<tol
        break; 
    end
end
for i = 1: 1000
    qC = exp(v(3)-p(3))/(1+sum(exp(v-p)));
    p(3) = 1/(1-qC);
    f3val = qC - exp(v(3)-p(3))/(1+sum(exp(v-p)));
    if abs(f3val)<tol
        break; 
    end
end    
    poldq3=pnewq3;
    pnewq3=p;
    j = j+1;
    if j >= 10000
        break;
    end
end
toc
%% Q4
% *Initial Guesses*
tic
p    = [1;1;1]; %ones(3,1);     % Country Price index, initial value      
v    = -ones(3,1);     % Coefficients to be estimated, initial guess
j       = 1;

tol = 1e-11 ; 
pnewq4 = p;
poldq4 = p+5;
while (norm(poldq4-pnewq4)>tol )
     p = pnewq4;
     for i = 1:1000
        q = exp(v-p)./(1+sum(exp(v-p)));
        p = ones(3,1)./(1-q);
        fval = norm(q-exp(v-p)./(1+sum(exp(v-p))));
        if fval<tol
            break; 
        end
     end
    poldq4=pnewq4;
    pnewq4=p;
    j = j+1;
    if j >= 10000
        break;
    end
end
toc
%% Q5

% *Initial Guesses*

p    = [3;2;1]; %ones(3,1);     % Country Price index, initial value      
v    = -ones(3,1);     % Coefficients to be estimated, initial guess


    qA = exp(v(1)-p(1))/(1+sum(exp(v-p)));
    p(1) = 1/(1-qA);
    qB = exp(v(2)-p(2))/(1+sum(exp(v-p)));
    p(2) = 1/(1-qB);
    qC = exp(v(3)-p(3))/(1+sum(exp(v-p)));
    p(3) = 1/(1-qC);   

p

% You need to repeat this in a loop until convergence. it took me 4 times
% until it converged. 



