function fval = HW4_third(beta,X,Y,Z)
%beta1 = beta
%beta2 = sigma_beta
%beta3 = gamma
%beta4 = u
%beta5 = sigma_ubeta
%beta6 = sigma_u
if beta(2)<0 || beta(5)<0 || beta(5)^2 - beta(2)*beta(6)>0
    fval = Inf;
else
N = size(X,2);
beta_sim = rand(100,N);
u = rand(100,N);

Likelihood2 = zeros(1,N);
    for ii = 1:100
      uu = repmat(u(ii,:),20,1);
      Likelihood_it = (((1./(1+exp(-(beta_sim(ii,:).*X+beta(3).*Z +uu)))).^Y).*...
     ((1-1./(1+exp(-(beta_sim(ii,:).*X+beta(3).*Z+uu)))).^(1-Y)));
      Likelihood_i = prod(Likelihood_it);
         for jj = 1:100
                sim_pdf(jj) = mvnpdf([beta_sim(ii,jj);u(ii,jj)],[beta(1);beta(4)],[beta(2),beta(5);beta(5),beta(6)]);
         end
     Likelihood2 = Likelihood2 + Likelihood_i.*sim_pdf;
    end
    fval = -sum(log(Likelihood2));
end
end