% Q1
X = [1,1.5,3,4,5,7,9,10]
Y1 = -2+.5*X
Y2 = -2 + .5*X.^2
figure(1)
hold on
plot(X,Y1);
plot(X,Y2);
xlabel('X');
ylabel('Y1 and Y2');
legend('Y1','Y2');
hold off

%Q2
X = -10:(30/199):20
sum(X)

%Q3
A = [2,4,6;2,8,6;3,12,4]
B = [-2;3;10]
C = A'*B
% inv is a slow function, use the \ operator from answer key
D = inv(A'*A)*B
E = sum(A.*B)
F = A(1:length(A):end)
x = inv(A)*B

%Q4
% learn to use kron()
B = blkdiag(A,A,A,A,A)

%Q5
A = 10+5.*randn(5,3)
% there is faster way to do that, check the answer key
for i = 1:size(A,1)
    for j = 1:size(A,2)
        if A(i,j)<10
            newA(i,j) = 0;
        elseif A(i,j)>=10
            newA(i,j) = 1;
        end 
    end 
end
newA

%Q6
% i need you to write data import routines so that the code works on my
% computer too. 
if istable(datahw1)
    datahw1 = table2array(datahw1);
end
Y = datahw1(:,5);
X = [ones(length(datahw1),1), datahw1(:,3),datahw1(:,4),datahw1(:,6)];
[beta,Sigma,E,CovB,logL] = mvregress(X,Y)
t_stat = beta./sqrt(diag(CovB))
p_value = 2.*(1-tcdf(t_stat,length(datahw1)-1))