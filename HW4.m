clc; clear all;
load('hw4data.mat')
N = data.N;
T = data.T;
X = data.X;
Y = data.Y;
Z = data.Z;
%% Q1
beta0 = 0.1;
sigma_beta = 1;
gamma = 0;
pd = makedist('Normal',beta0,sigma_beta);
beta_sim = randn(20,N)+beta0;
beta_sim_pdf = pdf(pd,beta_sim);
% This is not Gauss quadrature rules

Likelihood = zeros(1,N);
for ii = 1:20
Likelihood_it = (((1./(1+exp(-(beta_sim(ii,:).*X)))).^Y).*...
    ((1-1./(1+exp(-(beta_sim(ii,:).*X)))).^(1-Y)));
Likelihood_i = prod(Likelihood_it);
Likelihood = Likelihood + Likelihood_i.*beta_sim_pdf(ii,:);
end

Likelihood = sum(log(Likelihood))


%% Q2
beta_sim = rand(100,N);
beta_sim_pdf = pdf(pd,beta_sim);

Likelihood2 = zeros(1,N);
for ii = 1:100
Likelihood_it = (((1./(1+exp(beta_sim(ii,:).*X))).^Y).*((1-1./(1+exp(beta_sim(ii,:).*X))).^(1-Y)));
% this is not the right formula for likelihood calculation, misses u and z
Likelihood_i = prod(Likelihood_it);
Likelihood2 = Likelihood2 + Likelihood_i.*beta_sim_pdf(ii,:);
end

Likelihood2 = sum(log(Likelihood2))

%% Q3
%beta1 = beta
%beta2 = sigma_beta
%beta3 = gamma
%beta4 = u
%beta5 = sigma_ubeta
%beta6 = sigma_u
initial_value = [0.1,1,0]
[beta1, fval1] = fmincon(@(b)HW4_first(b,X,Y,Z),initial_value,[0,-1,0],0);
beta1
fval1
[beta2, fval2] = fmincon(@(b)HW4_second(b,X,Y,Z),initial_value,[0,-1,0],0);
beta2
fval2
% incorrect answers due to misspecified objective. see answer key
%% Q4

initial_value = [.1,1,0,1,0,1]
nonlin = @nonlincon;
% my matlab cannot inteprter this condition, maybe it was on your computer
% somwhere and you forgot to include it in repository.
options = optimoptions('fmincon','Display','iter','Algorithm','sqp');
[beta3,fval3] = fmincon(@(b)HW4_third(b,X,Y,Z),initial_value,[0,-1,0,0,0,-1],0,[],[],[],[],nonlin,options);
beta3
fval3
% does not run due to non-existent nonlincon.
