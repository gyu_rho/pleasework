clear
addpath('C:/Users/Gyu/Documents/econ_512_2017/CEtools/')
% Do not use absolute path, use relative path
%% PART A
theta1_new = 10;
theta1_old = 10;
max_iter = 1000;
Y = 1.1:0.01:3;
theta1 = zeros(length(Y),1);
for jj = 1:length(Y)
    for ii = 1:max_iter
        theta1_new = theta1_old - (log(theta1_old)-log(Y(jj))-psi(theta1_old))/(1/theta1_old - trigamma(theta1_old));
        if abs(theta1_new-theta1_old)<eps
            break;
        else
            theta1_old = theta1_new;
        end
    end
    theta1(jj) = theta1_new;
end
figure(1)
hold on
plot(Y,theta1)
xlabel('$\theta_1$','interpreter','latex'); ylabel('$\displaystyle\frac{Y_1}{Y_2}$','interpreter','latex')
hold off

% this procedure does not seem to estimate theta

%% PART B

load('~/Google Drive/econ_512_2017/homework/hw3.mat');
% again, either put HW3.mat here or use relative path

% 1.
% FMINUNC
fun1 = @(beta)-sum(-exp(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])...
    +y.*(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])-log(factorial(y)));

options1 = optimoptions('fminunc','Display','iter');
beta1 = fminunc(fun1,[0;0;0;0;0;0],options1)

% FMINUNC WITH GRADIENT
options2 = optimoptions('fminunc','Display','iter','SpecifyObjectiveGradient',true);
beta2 = fminunc(@(beta) fun2(beta,X,y),[0;0;0;0;0;0],options2)

% FMINSEARCH
fun3 = @(beta)sum(-exp(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])...
    +y.*(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])-log(factorial(y)));

optset('neldmead','maxit',1086);
optset('neldmead','showiters',1);
beta3 = neldmead(fun3,[0;0;0;0;0;0])

% BHHH

new_beta4 = [0;0;0;0;0;0];
iter4 = 1;
while 1
      diff =  0;
        for ii = 1:length(y)
         diff = diff  + (X(ii,:)'*(-exp(X(ii,:)*[new_beta4(1);new_beta4(2);new_beta4(3);new_beta4(4)...
             ;new_beta4(5);new_beta4(6)]))+X(ii,:)'*y(ii));
        end
    S = 0;
    for ii = 1:length(y)
        s = (X(ii,:)'*(-exp(X(ii,:)*[new_beta4(1);new_beta4(2);new_beta4(3);new_beta4(4);new_beta4(5);new_beta4(6)]))...
    +X(ii,:)'*y(ii));
        S = S - s*s';
    end
    beta_temp = new_beta4 - S\diff;
    old_beta4 = new_beta4;
    new_beta4 = beta_temp;
    if norm(new_beta4 - old_beta4)<1e-12
        break; 
    end
    iter4 = iter4 + 1;
end
beta4 = new_beta4 
iter4

% 2.
H = 0;
for ii = 1:length(y)
    H = H +  X(ii,:)'*(X(ii,:)*(-exp(X(ii,:)*[beta4(1);beta4(2);beta4(3);beta4(4)...
             ;beta4(5);beta4(6)])));
end

eH = eig(H) %eigenvalue of Hessian
eS = eig(S) %eigenvalue of Approx

% 3.
% 
% fun1 = @(beta)sum((y-exp(X*[beta(1);beta(2);beta(3);beta(4);beta(5);beta(6)])).^2);
% options1 = optimoptions('fminunc','Display','iter');
% beta11 = fminunc(fun1,[0;0;0;0;0;0],options1)

new_beta5 = [0;0;0;0;0;0];
iter5 = 1;
while 1
    J = zeros(length(y),length(beta1));
    for ii = 1:length(y)
        for jj = 1:length(beta1)
            J(ii,jj) = -X(ii,jj)*(exp(X(ii,:)*[new_beta5(1);new_beta5(2);new_beta5(3);new_beta5(4)...
             ;new_beta5(5);new_beta5(6)]));
        end
    end
    f = zeros(length(y),1);
    for ii = 1:length(y)
       f(ii) = (y(ii)-exp(X(ii,:)*[new_beta5(1);new_beta5(2);new_beta5(3);new_beta5(4)...
             ;new_beta5(5);new_beta5(6)])); 
    end
    beta_temp = new_beta5 - (J'*J)\J'*f;
    old_beta5 = new_beta5;
    new_beta5 = beta_temp;
    if norm(new_beta5 - old_beta5)<1e-12
        break; 
    end
    iter5 = iter5 + 1;
end

beta5 = new_beta5 
iter5
% 4. 

std_BHHH = diag(sqrt(inv(-H)./length(y))) %Standard deviation for BHHH

D = zeros(6,6);
V = 0;
for ii = 1:length(y)
    D = D + (X(ii,:)'.*exp(X(ii,:)*beta5)^2.*X(ii,:));
    V = V + (y(ii) - exp(X(ii,:)*beta5))^2.*(X(ii,:)'.*exp(X(ii,:)*beta5)^2.*X(ii,:));
end
V = 4/length(y).*V;
D = 1/length(y).*D;
psi = D\V/D;
std_NLLS = diag(sqrt(inv(psi)./length(y))) %Standard Deviation for NLLS

